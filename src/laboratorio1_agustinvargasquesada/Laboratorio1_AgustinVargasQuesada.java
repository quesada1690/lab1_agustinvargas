package laboratorio1_agustinvargasquesada;

import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author varga
 */
public class Laboratorio1_AgustinVargasQuesada {

    static HashMap diccionario_lleno = new HashMap(); //para guardar el return

    public static void main(String[] args) {
        int opcion;
        Scanner eleccion = new Scanner(System.in);
        
        do //menu con do/while para poder enciclar y que se salga hasta que el usuario quiera
        {
            System.out.println("|                       Menú principal                |");
            System.out.println("|          Digite la opcion que desee utilizar        |");
            System.out.println("|                        1-Jugar                      |");
            System.out.println("|                    2-Tabla de posiciones            |");
            System.out.println("|                        3-Reporte                    |");
            System.out.println("|                        4-Salir                      |");
            opcion = eleccion.nextInt();
            switch (opcion) {
                case 1:
                    clase_opcionJugar_listas emparejar = new clase_opcionJugar_listas();
                    diccionario_lleno = emparejar.jugar();
                    break;

                case 2:
                    clase_tabla_posiciones tabla = new clase_tabla_posiciones();
                    tabla.Tabla_posiciones(diccionario_lleno);
                    break;

                case 3:
                    clase_reporte resumen = new clase_reporte();
                    resumen.reporte(diccionario_lleno);
                    break;

                case 4:
                    System.out.println("Que tenga un buen dia");
                    break;

                default:
                    System.out.println("Digite las opciones dadas");
                    break;

            }
        } while (opcion != 4);
    }
}
